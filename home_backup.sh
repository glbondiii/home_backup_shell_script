#!/bin/sh

if [ -z "$1" ]
then 
	input="./~_Backup.txt"
else
	input=$1
fi

if [ -d $HOME/'~_Backup' ]; then
	rm -rf $HOME/'~_Backup'
fi

mkdir $HOME/'~_Backup'

while IFS= read -r line
do 
	cp ~/$line -rv $HOME/'~_Backup'
	if [ -f ~/$line ] || [ -d ~/$line ]; then
		echo "$line has been backed up"
	fi
done < "$input"

