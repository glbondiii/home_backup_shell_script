#!/bin/sh

if [ ! -d $HOME/'~_Backup' ]; then
	echo "Error: ~/Backup file not found in $HOME"
else 
	cp -rv $HOME/'~_Backup'/. $HOME
fi
