# Home Backup

This a a bash/zsh script that I made that backs up select files from the home directory.

How to use: 
1. Clone this git repo into your home directory  
2. Run "chmod +x home_backup.bash" in your terminal 
> Note: if using zsh (default on macOS 10.15 and later), then replace ".bash" with ".zsh"
3. Put the name of any files or directories you want to backup in the '~_Backup.txt' file
4. Run "./home_backup.bash" in your terminal
> (See Step 2 Note)
5. Profit

How to open backup:
1. Make sure that a '~_Backup' directory exists in your home directory
2. Run "chmod +x open_backup.bash" in your terminal 
> (See Step 2 Note From "How to use")
3. Run "./open_backup.bash" in your terminal
> (See Step 2 Note)
4. Profit
